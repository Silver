#include "chatterbox.h"

struct Silver_Conf configuration;
GtkWidget *window;
GtkWidget *menubar;
GtkWidget *vbox;
GtkWidget *hbox;
GtkWidget *hbox2;
GtkWidget *channelslist;
GtkWidget *nickslist;
GtkWidget *vbox2;
GtkWidget *chatlog;
GtkWidget *speakbox;
GtkWidget *channeltitle;
GtkWidget *statusbar;

// Widgets for the menu
GtkWidget *menu_file;
GtkWidget *item_file;
GtkWidget *item_quit;

void init_chatterbox()
{
	if (load_config(&configuration, "chatterbox") == 1)
		printf("Error: Could not load configuration file.\n");

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Chatterbox");
	gtk_window_set_default_size(GTK_WINDOW(window), 640, 420);
	g_signal_connect(window, "destroy", G_CALLBACK(quit_chatterbox), NULL);
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);
	
	menubar = gtk_menu_bar_new();
	gtk_box_pack_start(GTK_BOX(vbox), menubar, FALSE, TRUE, 0);
	
	menu_file = gtk_menu_new();
	item_file = gtk_menu_item_new_with_label("File");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_file), menu_file);
	gtk_menu_bar_append(GTK_MENU_BAR(menubar), item_file);
	
	item_quit = gtk_menu_item_new_with_label("Quit");
	gtk_menu_shell_append(GTK_MENU_SHELL(menu_file), item_quit);
	
	hbox = gtk_hpaned_new();
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
	
	channelslist = gtk_tree_view_new();
	gtk_paned_add1(GTK_PANED(hbox), channelslist);
	
	hbox2 = gtk_hpaned_new();
	gtk_paned_add2(GTK_PANED(hbox), hbox2);
	
	vbox2 = gtk_vbox_new(FALSE, 0);
	gtk_paned_add1(GTK_PANED(hbox2), vbox2);
	
	nickslist = gtk_tree_view_new();
	gtk_paned_add2(GTK_PANED(hbox2), nickslist);
	
	channeltitle = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(vbox2), channeltitle, FALSE, TRUE, 0);
	
	chatlog = gtk_text_view_new();
	gtk_box_pack_start(GTK_BOX(vbox2), chatlog, TRUE, TRUE, 0);
	
	speakbox = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(vbox2), speakbox, FALSE, TRUE, 0);
	
	statusbar = gtk_statusbar_new();
	gtk_box_pack_start(GTK_BOX(vbox), statusbar, FALSE, TRUE, 0);
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "Chatterbox"), "Welcome to Chatterbox");

	gtk_widget_show_all(window);
}

void quit_chatterbox()
{
	delete_config(&configuration);
	gtk_main_quit();
}
