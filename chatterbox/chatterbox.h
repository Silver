#ifndef CHATTERBOX_H
#define CHATTERBOX_H

#include <gtk/gtk.h>
#include <silver/libsilverconf.h>
#include <glib.h>

void init_chatterbox();
void quit_chatterbox();

#endif // CHATTERBOX_H
