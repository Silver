#include "confeditor.h"

struct Silver_Conf configuration;
GtkWidget *window;
GtkWidget *menubar;
GtkWidget *hbox;

void init_confeditor()
{
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Silver Configuration Editor");
	gtk_window_set_default_size(GTK_WINDOW(window), 640, 520);
	g_signal_connect(window, "destroy", G_CALLBACK(quit_confeditor), NULL);
	
	hbox = gtk_hpaned_new();
	gtk_container_add(GTK_CONTAINER(window), hbox);
	
	gtk_widget_show_all(window);
}

void quit_confeditor()
{
	gtk_main_quit();
}
