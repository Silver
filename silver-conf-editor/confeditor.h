#ifndef CONFEDITOR_H
#define CONFEDITOR_H

#include <gtk/gtk.h>
#include <silver/libsilverconf.h>
#include <glib.h>

void init_confeditor();
void quit_confeditor();

#endif // CONFEDITOR_H
