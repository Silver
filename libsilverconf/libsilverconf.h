#ifndef LIBSILVERCONF_H
#define LIBSILVERCONF_H

struct Silver_Conf
{
	const char *appname;
	char *path;
};

int load_config(struct Silver_Conf *conf, const char *appname);
const char *get_config(struct Silver_Conf *conf, const char *section, const char *key, const char *default_value);
void delete_config(struct Silver_Conf *conf);

#endif // LIBSILVERCONF_H
