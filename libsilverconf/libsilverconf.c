#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "libsilverconf.h"

int load_config(struct Silver_Conf *conf, const char *appname)
{
	struct stat st;
	int result = 0;

	conf->appname = appname;

	char *path = malloc(300);
	strcpy(path, getenv("HOME"));
	if (stat(path, &st) != 0)
		result = 1;

	strcat(path, "/.config");
	if (stat(path, &st) != 0)
	{
		int res = mkdir(path, 0755);
		if (result == 0)
			return res;
		if (res != 0)
			printf("%s: Error creating directory %s. Error code: %i", appname, path, res);
	}
		
	strcat(path, "/silver");
	if (stat(path, &st) != 0)
	{
		int res = mkdir(path, 0755);
		if (result == 0)
			return res;
		if (res != 0)
			printf("%s: Error creating directory %s. Error code: %i", appname, path, res);
	}

	strcat(path, "/config/");
        if (stat(path, &st) != 0)
        {
                int res = mkdir(path, 0755);
                if (result == 0)
                        return res;
                if (res != 0)
                        printf("%s: Error creating directory %s. Error code: %i", appname, path, res);
        }

	strcat(path, appname);
	if (stat(path, &st) != 0)
	{
		int res = mkdir(path, 0755);
		if (result == 0)
			return res;
		if (res != 0)
			printf("%s: Error creating directory %s. Error code: %i", appname, path, res);
	}

	conf->path = path;
	
	return result;
}

const char *get_config(struct Silver_Conf *conf, const char *section, const char *key, const char *default_value)
{
	const char *value = default_value;
	
	return value;
}

void delete_config(struct Silver_Conf *conf)
{
	free(conf->path);
	conf->path = NULL;
}
